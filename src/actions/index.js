import * as types from './ActionTypes';

export function addPhone(name, number, birthday){
    return{
        type : types.ADD_PHONE,
        name,
        number,
        birthday,
    }
}

export function setSelected(selected) {
    return{
        type : types.SET_SELECTED,
        selected,
    };
}
export function deletePhone() {
    return{
        type : types.DELETE_PHONE,
    };
}