import { combineReducers } from 'redux';
import managePhone from './managePhone';

const reducers = combineReducers({
    managePhone
});

export default reducers;