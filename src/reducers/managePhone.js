import * as types from '../actions/ActionTypes';

const initialState = {
    selected: 0,
    phonelist: [
     {
       name : "ㅇㅂㅇ", 
       number:"010-0000-0000",
       birthday:"03/20"
     }, 
     {
       name:"ㅇ6ㅅㅇ", 
       number:"010-0000-0001", 
       birthday:"02/08"
     },
     {
         name: "ㅎㅅㅎ",
         number: "000-2222-1111",
         birthday: "df/asdf"
     }
   ],
   jointable : [
       {
           I:'0',
           friend:'0',
        },
       {
           I:'0',
           friend:'2',
        },
       {
           I:'2',
           friend:'1',
        }
    ],
};

function managePhone (state = initialState, action) {
    switch(action.type) {
        case types.ADD_PHONE:
            return { 
                ...state,  
                phonelist: [...state.phonelist, {name: action.name, number :action.number, birthday: action.birthday}],
            };
        case types.DELETE_PHONE:
            return{
                phonelist : state.phonelist.filter((name, index) => index !== state.selected),
                selected : (state.phonelist.length - 1 == state.selected) ? state.selected - 1 : state.selected,
            };
        case types.SET_SELECTED:
            return{
                ...state,
                selected : action.selected,
            };
        default:
            return state;
    }
}

export default managePhone;