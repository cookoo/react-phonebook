import React, { Component } from 'react';
import '../App.css';
import InputField from './InputField'
import PhoneList from'./PhoneList'
import ShowInfoField from './ShowInfoField'
import FriendsList from './FriendsList'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { addPhone, setSelected, deletePhone } from '../actions/index';
import MyTypes from '../MyTypes';

function createWarning(funcName) {
  return () => console.warn(funcName + ' is not defined');
}

class Phonebook_ extends Component{

  static propTypes = {
    onAdd: PropTypes.func.isRequired,
    onSetSelected: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    phonelist: PropTypes.arrayOf(MyTypes.Contact).isRequired,
    selected: PropTypes.number.isRequired,
    jointable: PropTypes.arrayOf(MyTypes.Relationship.isRequired).isRequired,
  };

  static defaultProps = {
    onAdd: createWarning('onAdd'),
    onSetSelected: createWarning('onSetSelected'),
    onDelete: createWarning('onDelete'),
  };
  

  constructor(props){
    super(props);
  }

  render(){
    return (
      <div className="container">
        <div className="panelone">
          <InputField />
          <PhoneList 
            onSetSelected = {this.props.handleSetSelected}
            phonelist = {this.props.phonelist}
            />
        </div>
        <div className="panelone">
          <ShowInfoField 
            onDelete = {this.props.handleDelete}
            phonelist = {this.props.phonelist}
            selected = {this.props.selected}
          />
        </div>
        <div>
          <FriendsList 
            phonelist = {this.props.phonelist}
            selected = {this.props.selected}
            jointable = {this.props.jointable}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  phonelist: state.managePhone.phonelist,
  selected : state.managePhone.selected,
  jointable : state.managePhone.jointable,
});

const mapDispatchToProps = (dispatch) => ({
  handleDelete : () => {dispatch(actions.deletePhone())},
  handleSetSelected : (selected) => {dispatch(actions.setSelected(selected))},
})


/**
 * handleAdd : 새로운 name, number, birthday를 받아서 phonelist에 추가한다
 * handleDelete : phonelist[selected]를 삭제한다
 * handleShowAll : 모든 phonelist의 name, number를 화면에 출력한다.
 * handleShowFriends : selected의 friends의 name, number를 출력한다.
 * handleShowInfo : selected의 name, number, birthday를 출력한다.
 * handleSelected : selected를 설정한다.
 * @param {*} dispatch 
 */



const Phonebook =  connect(mapStateToProps, mapDispatchToProps)(Phonebook_);

export default Phonebook;
