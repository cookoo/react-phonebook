import React, { Component } from 'react';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import * as actions from '../actions';

const propTypes = {
  onAdd: PropTypes.func.isRequired,
  onSetSelected: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  phonelist: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      number: PropTypes.number.isRequired,
      birthday: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  selected: PropTypes.number.isRequired,
  jointable: PropTypes.arrayOf(
    PropTypes.shape({
      I: PropTypes.number.isRequired,
      friend: PropTypes.number.isRequired,
    }).isRequired
  ).isRequired,
};

class InputField extends Component{
    
    

    state = {
      name : "noname",
      number : "010-0000-0000",
      birthday : "00/00",
    }
    
    // 변화값 state에 저장
    handleChange = input => e => {
        this.setState({[input]: e.target.value});
    }


    render(){

        return(
            <>           
            <TextField
              label="Name"
              floatingLabelText="Name"
              onChange={this.handleChange('name')}
            />
            <TextField
              label="Phone Number"
              floatingLabelText="Number"
              onChange={this.handleChange('number')}
            />
            <TextField
              label="Birthday"
              floatingLabelText="Birthday"
              onChange={this.handleChange('birthday')}
            />
            <br />
            <Button 
              variant="contained" 
              color="primary" 
              onClick={() => this.props.onAdd(this.state.name, this.state.number, this.state.birthday)}>
              Add
            </Button>
          </>
        );
    }
}
InputField.propTypes = propTypes;

const mapDispatchToProps = (dispatch) => ({
  onAdd : (name, number, birthday) => {dispatch(actions.addPhone(name, number, birthday))},
})

export default connect(null, mapDispatchToProps)(InputField);
