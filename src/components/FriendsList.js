import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

const propTypes = {
    selected: PropTypes.number.isRequired,
    phonelist: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string.isRequired,
          number: PropTypes.number.isRequired,
          birthday: PropTypes.string.isRequired,
        }).isRequired,
      ).isRequired,
    jointable: PropTypes.arrayOf(
        PropTypes.shape({
          I: PropTypes.number.isRequired,
          friend: PropTypes.number.isRequired,
        }).isRequired,
      ).isRequired,
}

class FriendsList extends Component
{
    showList = (a) => {
        // if(!a) return; // debug
        // else if(!a.name) return; //debuga

        return (   
          <Paper>
            <Grid container wrap="nowrap" spacing={16} >
              <Grid item>
               <Avatar>{a.name[0]}</Avatar>
              </Grid>
              <Grid item xs zeroMinWidth>
               <Typography noWrap>{a.name} {a.number}</Typography>
              </Grid>
           </Grid>
          </Paper>
        );
    }

    showFriendList = (index, list) => {
        if(!this.props.jointable) return;
        const friendIndexList = (this.props.jointable.filter((me) => me.I == index)).map((mee, id) => mee.friend);
        const friendList = friendIndexList.map((item, friendID) => list[friendID]);
        
        return(
            <div>
                {friendList.map((a) => this.showList(a))}
            </div>
        );
    }

    render() {
        return(
            <div>
                {this.showFriendList(this.props.selected, this.props.phonelist)}
            </div>
        );
    }
}

FriendsList.propTypes = propTypes;

export default FriendsList;
