import React, { Component } from 'react';
import '../App.css';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const propTypes = {
    selected: PropTypes.number.isRequired,
    phonelist: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string.isRequired,
          number: PropTypes.number.isRequired,
          birthday: PropTypes.string.isRequired,
        }).isRequired,
      ).isRequired,
    
}

class PhoneList extends Component {
    // 리스트를 보여줌. : SHOW_ALL

    constructor(props) {
        super(props);
    }

    showList = (a, index) => {

        return (   
          <Paper key={index} onClick={() => this.props.onSetSelected(index)}>
            <Grid container wrap="nowrap" spacing={16} >
              <Grid item>
               <Avatar>{a.name[0]}</Avatar>
              </Grid>
              <Grid item xs zeroMinWidth>
               <Typography noWrap>{a.name} {a.number}</Typography>
              </Grid>
           </Grid>
          </Paper>
        );
    }

    render(){
        return(
            <div>
                {this.props.phonelist.map((a, index) => this.showList(a, index))}
            </div>
        );
    }

}

PhoneList.propTypes = propTypes;


export default PhoneList;
