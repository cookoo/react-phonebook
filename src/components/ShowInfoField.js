import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import PropTypes from 'react';

class ShowInfoField extends Component {
    // SHOW_INFO
    showInfo = (a) => {
        //다른 레이아웃으로 바꾸기
        if(!a) return null;
        return (   
             <Paper>
              <Grid container wrap="nowrap" spacing={16}>
                <Grid item xs = {10}>
                 <Typography>
                  <h1>{a.name}</h1> 
                  <div>{a.number}</div>
                  <p>{a.birthday}</p>
                 </Typography>
                </Grid>
                 <Grid item>
                </Grid>
              </Grid>
             </Paper>
        );
    }

    render(){
        // phonelist와 selected 가져옴
        return(
            <>
                {this.showInfo(this.props.phonelist[this.props.selected])}
                <Button variant="contained" color="secondary" onClick={() => this.props.onDelete()}>
                    Delete
                </Button>
            </>
        );
    }
}

export default ShowInfoField;
