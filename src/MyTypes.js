import PropTypes from 'prop-types';

const types = {
    Contact: PropTypes.shape({
        name: PropTypes.string.isRequired,
        number: PropTypes.number.isRequired,
        birthday: PropTypes.string.isRequired
    }),
    Relationship: PropTypes.shape({
        I: PropTypes.number.isRequired,
        friend: PropTypes.number.isRequired,
    }),
}
export default types;